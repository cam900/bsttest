#include <iostream>

template<class T>
struct BSTNode
{
	T data;
	BSTNode<T>* left;
	BSTNode<T>* right;

	BSTNode(T d)
		: data(d)
		, left(nullptr)
		, right(nullptr)
	{
	}
};

template<class T>
class BinarySearchTree
{
private:
	BSTNode<T>* root = nullptr;

public:
	~BinarySearchTree()
	{
		delete_tree();
	}

	void insert(T val)
	{
		if (!root) // root가 null이면
		{
			root = new BSTNode<T>(val);
		}
		else
		{
			insert_impl(root, val);
		}
	}

	void erase(T val)
	{
		root = erase_impl(root, val);
	}

	BSTNode<T>* find(T val)
	{
		return find_impl(root, val);
	}

	void inorder()
	{
		inorder_impl(root);
	}

	void delete_tree()
	{
		delete_tree_impl(root);
	}

private:
	// 노드 삽입
	bool insert_impl(BSTNode<T>* curr, T value)
	{
		if (value == curr->data)
		{
			return false;
		}
		else if (value < curr->data)
		{
			if (!curr->left) // curr->left null이면
			{
				curr->left = new BSTNode<T>(value);
				return true;
			}
			else
			{
				return insert_impl(curr->left, value);
			}
		}
		else if (value > curr->data)
		{
			if (!curr->right) // curr->right null이면
			{
				curr->right = new BSTNode<T>(value);
				return true;
			}
			else
			{
				return insert_impl(curr->right, value);
			}
		}
		return false;
	}

	BSTNode<T>* successor(BSTNode<T>* node)
	{
		auto curr = node->right;
		while (curr && curr->left)
		{
			curr = curr->left;
		}
		return curr;
	}

	// 노드 삭제후, 부모 노드 포인터가 가리켜야할 노드의 주소를 반환
	BSTNode<T>* erase_impl(BSTNode<T>* node, T value)
	{
		if (!node)
			return nullptr;
		if (value < node->data)
		{
			node->left = erase_impl(node->left, value);
		}
		else if (value > node->data)
		{
			node->right = erase_impl(node->right, value);
		}
		else
		{
			if (node->left && node->right) // 왼쪽, 오른쪽 자식이 있는경우
			{
				auto success = successor(node);
				node->data = success->data;
				node->right = erase_impl(node->right, success->data);
			}
			else
			{
				// 자식노드가 없거나 하나만 있는 경우
				auto tmp = node->left ? node->left : node->right;
				delete node;
				return tmp;
			}
		}
		return node;
	}

	// 값 탐색
	BSTNode<T>* find_impl(BSTNode<T>* curr, T value)
	{
		if (curr == nullptr)
		{
			return nullptr;
		}
		else if (curr->data == value)
		{
			return curr;
		}
		else
		{
			if (value < curr->data)
			{
				return find_impl(curr->left, value);
			}
			else // if (value > curr->data)
			{
				return find_impl(curr->right, value);
			}
		}
	}
	
	void inorder_impl(BSTNode<T>* curr)
	{
		if (curr)
		{
			inorder_impl(curr->left);
			std::cout << curr->data << ", ";
			inorder_impl(curr->right);
		}
	}

	void delete_tree_impl(BSTNode<T>* curr)
	{
		if (curr)
		{
			delete_tree_impl(curr->left);
			delete_tree_impl(curr->right);
			delete curr;
		}
	}
};